package com.obrien.practice.hibernate.demo;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.obrien.practice.hibernate.entity.Employee;

public class HibernatePracticeDemo {

	private static final Logger LOG = LoggerFactory.getLogger(HibernatePracticeDemo.class);
	private static SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml")
			.addAnnotatedClass(Employee.class).buildSessionFactory();

	public static void main(String[] args) {
		try {
			int noOfEmployeesCreated = populateDB();

			verifyDB(noOfEmployeesCreated);

			findEmployeeById(1);

			findEmployeeByCompanyName("TechStar");

			updateEmployee();

			deleteEmployee();

			clearDB();

			LOG.info("DONE!!!!!!!!!!!!!!!!!!!");
		} finally {
			sessionFactory.close();
		}
	}
	
	private static int  populateDB() {
		Employee employ1 = new Employee("Bob", "Kelly", "TechStar");
		Employee employ2 = new Employee("Patrick", "Murphy", "Huawei");
		Employee employ3 = new Employee("Mary", "Smith", "Topaz");

		Session sesh = sessionFactory.getCurrentSession();
		sesh.beginTransaction();

		sesh.save(employ1);
		sesh.save(employ2);
		sesh.save(employ3);

		try {
			sesh.getTransaction().commit();
			LOG.info("Added 3 employees to the DB: {} {} {} ", employ1.getFirstName(), employ2.getFirstName(),
					employ3.getFirstName());
		} catch (Exception ex) {
			new Exception("There was a problem populating the DB", ex.getCause());
		}
		//Returning 3 as that is the number of employees created
		return 3;
	}
	
	private static void verifyDB(int noOfEmployeesCreated) {
		Session sesh = beginTrx();
		List<Employee> persistedList = sesh.createQuery("FROM Employee").getResultList();
		LOG.info("The DB should contain {}, and it contains {}", noOfEmployeesCreated, persistedList.size());
		if (noOfEmployeesCreated != persistedList.size()) {
			throw new AssertionError("The DB should contain " + noOfEmployeesCreated + " whereas it contains " + persistedList.size());
		}
		try {
			sesh.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static Employee findEmployeeById(int primaryKey) {
		Session sesh = beginTrx();
		Employee persistedEmployee = sesh.get(Employee.class, primaryKey);

		LOG.info("Retrieved this Employee from the DB {}", persistedEmployee);
		commitTrxAndCloseSesh(sesh);
		return persistedEmployee;
	}

	private static void findEmployeeByCompanyName(String company) {
		Session sesh = beginTrx();
		//from Student s where s.lastName='Doe'"
		List<Employee> techStarEmployees = sesh.createQuery("from Employee e where e.company='TechStar'")
				.getResultList();
		LOG.info("There are {} TechStar employees ", techStarEmployees.size());
		for (Employee e : techStarEmployees) {
			LOG.info("Employee's name {} {}", e.getFirstName(), e.getLastName());
		}
		try {
			sesh.getTransaction().commit();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}


	private static void updateEmployee() {
		Session sesh = beginTrx();
		String firstName = "Billy-Joe";
		int employeeId = 2;

		Employee tempEmployee = sesh.get(Employee.class, employeeId);
		tempEmployee.setFirstName(firstName);
		sesh.save(tempEmployee);
		LOG.info("Changing employee's firstName [{}] ", tempEmployee);
		commitTrxAndCloseSesh(sesh);
		
		verifyEmployeesFirstName(firstName, employeeId);

	}

	private static void verifyEmployeesFirstName(String firstName, int employeeId) throws AssertionError {
		Session sesh;
		Employee tempEmployee;
		sesh = beginTrx();
		tempEmployee = sesh.get(Employee.class, employeeId);
		LOG.info("Retrieving same Employee for verification [{}]", tempEmployee);
		
		if(!tempEmployee.getFirstName().equals(firstName)) {
			throw new AssertionError("Employee's name was not updated");
		}
		commitTrxAndCloseSesh(sesh);
	}


	private static void deleteEmployee() throws AssertionError {
		Session sesh = beginTrx();
		LOG.info("Deleting employee where id is 1");
		sesh.createQuery("delete FROM Employee WHERE id=1").
		executeUpdate(); 
		commitTrxAndCloseSesh(sesh);

		LOG.info("Deleted employee with id  is 1");

		sesh = sessionFactory.getCurrentSession();
		sesh.beginTransaction();
		Employee deletedEmployee = sesh.get(Employee.class,	1);
		if(deletedEmployee != null) {
			throw new
			AssertionError("This employee should have been deleted.");
		}
		commitTrxAndCloseSesh(sesh);

	}

	private static void clearDB() {
		Session sesh = beginTrx();

		Query truncateQuery = sesh.createSQLQuery("truncate table employee");
		try {
			truncateQuery.executeUpdate();

		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private static Session beginTrx() {
		Session sesh = sessionFactory.getCurrentSession();
		sesh.beginTransaction();
		return sesh;
	}


	private static void commitTrxAndCloseSesh(Session sesh) {
		try {
			sesh.getTransaction().commit();
			sesh.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

}
